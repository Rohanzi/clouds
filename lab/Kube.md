# Kubernetes как интерфейс для управления Docker

    Kubernetes - система с открытым исходным кодом для автоматического развертывания, расширения и управления контейнеризированными сервисами. 
    Все это сейчас в совокупности называется оркестровкой контейнеров.

## Обзор
### Возможности

- Предоставляет схожую с Docker swarm отказоустойчивость и распределения реплик контейнеров, но позволяет назначать определнные контейнеры только на определенные ноды
- Сервисы могут открывать порты во внешнюю сеть или внутреннюю сеть
- Расширяется аддонами и плагинами
- Предоставляет CLI и web GUI, а так же богатый API для управления контейнерами Docker.
- Встроенный фаервол
- Квоты на ресурсы
- `PersistentVolume` как абстракция для использования дискового хранилища на разных облачных сервисах
- `Secrets` - маленькие шифрованные хранилища для каких-то данных вроде ssh ключей
- `Rolling updates` - обновления сервисов без прерывания

### Терминология

Исключим уже известные понятия вроде контейнеров и кластера. Обсуждается только специфичная для Kubernetes терминология.

- **Deployment**
  Объект API, управляющий реплицированным приложением. Каждая реплика - Pod, Pod-ы распределены по нодам кластера. Можно назвать аналогом сервиса в Docker.
- **Service**
  Объект API, описывающий как обращаться к приложениям, таким как набор Pod-ов, а так же описывающий распределителей нагрузки и порты. Сервис может быть как внешним (публичный), так и внутренним (для использования внутри Kubernetes).
- **Pod**
  Самый маленький объект Kubernetes. Pod представляет из себя набор контейнеров в кластере (но может состоять и из одного контейнера, а так же иметь контейнеры с дополнительным функционалом, например логированием). Pod-ы обычно управляются Deployment-ами.

### Установка

#### Варианты

Среди локальных способов (кластер из одной ноды для разработчиков и поиграться) наиболее популярный Minikube (устанавливает с виртуальной машиной готовую среду Kubernetes) и Kubeadm-dind (Kubernetus в Docker in Docker). 

Canonical поддерживают свой особый способ создания инфраструктуры, как и CoreOS. Присутвуют VM-образы, есть и другие способы установки на голое железо. Сейчас Kubernetes предлагают использовать новое решение для ускорее установки на голое железо, называемое [Kubeadm](https://kubernetes.io/docs/setup/independent/install-kubeadm/), но пока не советуют использовать его на продакшене.

У Kubernetes есть довольно серьезные требования к железу и особенности.

**Требования Kubernetes**
- 2 ГБ ОЗУ или больше на каждой машине (меньше ОЗУ оставит непозволительно мало места для контейнеров и они могут выгружаться из памяти);
- 2 ядра ЦП или больше;
- уникальные hostname, MAC address, и product_uuid на каждой машине кластера;
- отключенный swap для правильной работы `kubelet` (основной сервис Kubernetes);
- открытые порты, указанные в документации и сетевое соединение между машинами кластера.

Из-за данных требований обучаться Kubernetes проще всего, используя локальное решение.


### Графический интерфейс

Графический интерфейс для Kubernetes доступен в виде Pod (контейнер).

![Kubernetes GUI](images/01.png)

В Minikube он предустановлен и доступен по команде `minikube dashboard`. Для других инсталляций его необходимо установить и воспользоваться `kubectl proxy`. 

#### Возможности GUI

- Deploy приложений (упрощенный со ссылкой на контейнер, а так же с YAML-описанием)
- Просмотр нагрузки на кластер, состояния Deployment-ов, состояния кластера
- Просмотр информации о сервисах
- Просмотр логов

В целом GUI удобен для управления и мониторинга продакшена, но неудобен для тестирования Deploymentов.

### Небольшое обучение для понимания Pods, Deployments, Services

#### Pod

Есть два метода создания Pod: описание через командную строку с указаним image контейнера или же через YAML-файл. В первом случае мы делаем Deployment сразу же, во втором получаем готовый YAML с описанием всех контейнеров одного Pod-а для последующего Deployment.

##### Описание в CLI

Как пример, сделаем Deployment с обучабщим образом:

```bash hl_lines="1"
$ kubectl run kubernetes-bootcamp --image=docker.io/jocatalin/kubernetes-bootcamp:v1 --p=8080
deployment "kubernetes-bootcamp" created
```

##### Описание в YAML

##### Pod с одним контейнером

Напишем простой файл-описание.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: nginx
spec:
  containers:
  - name: nginx
    image: nginx:1.7.9
    ports:
    - containerPort: 80
```

Дальше запустим Pod без Deployment. Учитывая, что Kubernetes умеет подгружать файлы с сети, а данное описание есть в примере, то воспользуемся этим.

```bash hl_lines="1 3"
$ kubectl create -f https://raw.githubusercontent.com/kubernetes/website/master/docs/user-guide/walkthrough/pod-nginx.yaml
pod "nginx" created
$ kubectl get pods
NAME                                   READY     STATUS              RESTARTS   AGE
kubernetes-bootcamp-6c74779b45-crm9c   1/1       Running             0          20m
nginx                                  0/1       ContainerCreating   0          19s
```

Учитывая, что мы не указали внешний порт (не Deployment), то воспользуемся прокси чтобы посмотреть что ответит nginx. В одной вкладке терминала напишем `kubectl proxy`, а в другой сделаем curl запрос по имени pod-а:

```bash hl_lines="1"
$ curl http://localhost:8001/api/v1/proxy/namespaces/default/pods/nginx/
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
```

##### Несколько контейнеров в Pod

Допустим, у нас вот такое описание и мы сделали с ним Pod.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: www
spec:
  containers:
  - name: nginx1
    image: nginx
  - name: nginx2
    image: nginx
```

```bash hl_lines="1 3"
$ kubectl create -f twonginx.yaml
pod "www" created
$ kubectl get pod www            
NAME      READY     STATUS    RESTARTS   AGE
www       1/2       Error     2          28s
```

Ошибка у нас потому что мы не указали никакой порт для nginx, но тем не менее нам и не нужен реально рабочий пример. Уже видно, что Pod состоит из двух контейнеров.

#### Deployment

Пришло время сделать Deployment. Deployment-ы позволяют нам обновлять релизы и предоставляют отказоустойчивое решение, однако без балансировки нагрузки. Чтобы завершить дело необходимо сделать сервис. Конфигурация будет такой:

```yaml
apiVersion: apps/v1beta1
kind: Deployment
metadata:
  name: nginx-deployment
spec:
  replicas: 2 # 2 пода (реплики)
  template: # создать поды по этой заготовке (можно было указать линк, например)
    metadata:
      # в отличие от pod-nginx.yaml, имя Pod не включается, а генерируется само из имени Deployment
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.7.9
        ports:
        - containerPort: 80
```

```bash hl_lines="1 3 6"
$ kubectl create -f https://raw.githubusercontent.com/kubernetes/website/master/docs/user-guide/walkthrough/deployment.yaml
deployment "nginx-deployment" created
$ kubectl get deployment 
NAME               DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
nginx-deployment   2         2         2            2           12s
$ kubectl get pods -l app=nginx
NAME                                READY     STATUS    RESTARTS   AGE
nginx-deployment-569477d6d8-g9hgj   1/1       Running   0          21s
nginx-deployment-569477d6d8-r6w2f   1/1       Running   0          21s
```

Как видим, наш Deployment состоит из двух реплик nginx. 

##### Обновление Deployment (не rolling updates)

Отредактируем наш YAML файл:

```yaml
apiVersion: apps/v1beta1
kind: Deployment
metadata:
  name: nginx-deployment
spec:
  replicas: 2
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.8 # обновление nginx с 1.7.9 до 1.8
        ports:
        - containerPort: 80
```

```bash hl_lines="1 2 7 13"
$ kubectl apply -f https://raw.githubusercontent.com/kubernetes/website/master/docs/user-guide/walkthrough/deployment-update.yaml
$ kubectl get pods -l app=nginx
NAME                                READY     STATUS              RESTARTS   AGE
nginx-deployment-569477d6d8-g9hgj   1/1       Running             0          6m
nginx-deployment-569477d6d8-r6w2f   1/1       Running             0          6m
nginx-deployment-f95b7b6b8-zwvr2    0/1       ContainerCreating   0          0s
$ kubectl get pods -l app=nginx
NAME                                READY     STATUS        RESTARTS   AGE
nginx-deployment-569477d6d8-g9hgj   0/1       Terminating   0          6m
nginx-deployment-569477d6d8-r6w2f   0/1       Terminating   0          6m
nginx-deployment-f95b7b6b8-mhrfn    1/1       Running       0          4s
nginx-deployment-f95b7b6b8-zwvr2    1/1       Running       0          27s
$ kubectl get pods -l app=nginx
NAME                               READY     STATUS    RESTARTS   AGE
nginx-deployment-f95b7b6b8-mhrfn   1/1       Running   0          1m
nginx-deployment-f95b7b6b8-zwvr2   1/1       Running   0          1m
```

Можно отследить как старые контейнеры постепенно заменялись на новые.

#### Service

Сервис объединяет Deployment под один IP-адрес и балансирует нагрузку. Можно сказать, что это делает готовый для использования сервис. Итак, вот наш файл с описанием примера:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx-service
spec:
  ports:
  - port: 8000 # the port that this service should serve on
    # the container on each pod to connect to, can be a name
    # (e.g. 'www') or a number (e.g. 80)
    targetPort: 80
    protocol: TCP
  # just like the selector in the deployment,
  # but this time it identifies the set of pods to load balance
  # traffic to.
  selector:
    app: nginx
```

Итак, создадим наш сервис и посмотрим его состояние:

```bash hl_lines="1 3"
$ kubectl create -f https://raw.githubusercontent.com/kubernetes/website/master/docs/user-guide/walkthrough/service.yaml   
service "nginx-service" created
$ kubectl get services
NAME            TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)    AGE
kubernetes      ClusterIP   10.0.0.1     <none>        443/TCP    3h
nginx-service   ClusterIP   10.0.0.53    <none>        8000/TCP   16s
```

Так как IP-адрес у нас использнуется внутри виртуальной машины, созданной Minikube, то посмотрим внутри нее работает ли сервис

```bash hl_lines="1 9"
host $ minikube ssh                    
                         _             _            
            _         _ ( )           ( )           
  ___ ___  (_)  ___  (_)| |/')  _   _ | |_      __  
/' _ ` _ `\| |/' _ `\| || , <  ( ) ( )| '_`\  /'__`\
| ( ) ( ) || || ( ) || || |\`\ | (_) || |_) )(  ___/
(_) (_) (_)(_)(_) (_)(_)(_) (_)`\___/'(_,__/'`\____)

VM $ curl 10.0.0.53:8000
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>
```

### Выводы

Рассмотренные в данной лабораторной работе с примерами возможности Kubernetes - всего лишь маленькая часть возможностей данного инструмента. Несомненно, Kubernetes очень гибкий инструмент для оркестровки контейнеров для продакшена, позволяющий:

- отлично управлять наборами контейнеров (Pod-ы), обновлять их и делать отказоустойчивость (Deployment-ы), а так же публиковать как готовые сервисы с балансировкой нагрузки (Service-ы);
- неплохой графический интервейс "изкоробки", богатый RESTfull API, консольный клиент;
- имеет встроенный прокси и богатый инструментарий для работы с сетью;
- легко создавать сервисы как внутренние, так и внешние;
- богатая работа с томами, поддержка секретных томов-файлов, облачных сервисов.

## Сравнение Kubernetes и Docker swarm

### Сравнение

#### Настройка кластера

Если посмотреть на документацию Kubernetes, то с самого начала становится понятно, что на каждой отдельной ОС придется знатно повозиться и узнавать что-то новое, часто гуглить и думать. Все потому что документация довольно скудная, а разработчики так и не предоставили официальных удобных методов установки сервиса. Кроме того у Kubernetes необходимо описывать ноды в кластере: назначать мастера выбирать контроллер, планировщик, выбрать из кучи решений одно для организации внутренней виртуальной сети между нодами и проч.

Docker же представляет на каждой машине единый API и решение Docker swarm для объединения машин в кластер. Настройка Docker swarm заметно проще, потому как у Docker один master и он сам решает как раскидывать сервисы (хотя мы можем при желании указать на отдельные машины).

**Победитель:** Docker

#### Расширяемость

В **Docker** за счет простоты API **деплой** контейнеров и обновление сервисов в больших и маленьких кластерах **происходит куда быстрее**, так как CLI довольно простой и понятный.

В Kubernetes достаточно сложный API и много тонкостей, потому деплой может происходить достаточно медленно. Тем более нужно описать целых 3 ступени приложения (Pod, Deploy, Service). 

**Победитель:** Docker

#### Высокая доступность

Оба сервиса предоставляют схожие механизмы реплицирования и избыточности сервисов. Кроме того система саморегулирующаяся и не нуждающаяся в управлении возвращенной в строй ноды.

**Победитель:** Оба хороши

#### Балансировка нагрузки

В Docker swarm балансировка нагрузки происходит автоматически, использу внутреннюю сеть swarm-а. Все обращения к любой машине автоматически перенаправляются "случайным" образом к какой-либо машине в кластере.

В Kubernetes обычно расрпеделение нагрузки идет через политики, описанные в других Pod-ах. Таким образом распределение нагрузки лежит на других сервисах. Для небольших кластеров это неудобно.

**Победитель:** За простотой и незатратностью - Docker. Если нужны сложные политики и имеется большой кластер - Kubernetes.

#### Обновления сервисов

Обновления сервисов в обоих системах происходят позожим образом: выкатываются контейнеры на новых Image и когда они заработают будут убраны контейнеры на старых Image. Таким образом работа сервсиса не прерывается (почти).

Однако Kubernetes поддерживает мониторинг сервисов (но его надо настроить) и более безопасен при таких обновлениях, потому как гарантирует, что работа сервиса не будет прервана (в конце концов обновление может откатиться обратно само).

**Победитель:** Kubernetes

#### Тома данных

В Docker работата с томами ведется как с локальными хранилищами или через плагины (чтобы работать с сетевыми расположениями). Тома всегда локальны на каждой ноде (как абстракция). Тома могут использоваться несколькими контейнерами, но это надо указать явно. Так же данные созраняются после изменения контейнеров.

В Kubernetes тома общие для всех таких же контейнеров (Pod-ов), общение происходит через абстракцию. Тома так же зависят от жизни Pod-ов и удалятся вместе с ними. Есть так же внешние тома, которые сохраняют данные в любом случае, но доступ так же для всех контейнеров в Pod, которые используют этот ресурс.

**Победитель:** Kubernetes

#### Сеть

У Docker swarm по умолчанию имеется одна (можно добаить еще) внутренняя сеть для межконтейнерная сеть для общения внутри кластера, защищенная TLS с сгенерированным сертификатами (можно использовать свои).

Внутренняя сеть конейнеров у Kubernetes реализуется с помощью плагинов-Pod-ов, один из наиболее популярных - flannel. Внутри Kubernetes так же есть своя внутренняя сеть, объявляемая через сервис etcd. Шифрование TLS так же доступно, но нуждается в ручной настройке. Из документации особенности работы сети Kubernetes могут быть не сразу понятны.

**Победитель:** Docker

#### Обнаружение сервисов

У Docker сервисы доступны через внутреннюю сеть кластера, притом Docker swarm сам роутит соединение. Доступна гибкая настройка IP-адресации внутри сети.

 Kubernetes опирается на etcd и ручное описание обнаружения сервисов. Для упрощения есть аддон DNS-сервера, а контейнеры умеет объявлять себя и добавлять информацию о себе в распределенную базу данных. В целом настройка обнаружения сервисов в Kubernetes более сложная.

**Победитель:** Docker

#### Мониторинг

Docker требует сторонних решений для мониторинга. То есть все ручками.

Kubernetes поддердивает мониторинг Elasticsearch/Kibana (ELK) для кластера в целом, а так же Heapster/Grafana/Influx для мониторинга сервисов контейнеров.

**Победитель:** Docker

#### Pods && Deployments && Services vs docker-compose

В Kubernetes система описания сервисов куда более сложная и многослойная, но в итоге напоминает docker-compose. 

В docker-compose описывается сразу работа с томами, используемые сети, объединяются контейнеры вместе. В последней версии compose 3.3 описываются так же параметры для docker swarm вроде количества реплик, до этого расширяемость была только через CLI. 

В терминологии Kubernetes вся та же функциональность обеспечивается Pods + Deployments + Services, но как бы плавно наслаивается. Pods описывают тома и контейнерное взаимодействие, Deployments описывают практически высокую доступность и сетевую составляющую, но это касается данной машины в кластере (как если docker-compose использовать без swarm). Services в Kubernetes описывают работу сервиса во всем кластере, отказоустойчивость и прочее.

**Победитель:** Docker более простой в целом и удобный, но иногда послойность Kubernetes может сыграть на руку, ибо позволяет послойно придти к высокой доступности.

### Выводы

* Docker Swarm
  * За
    * Легок в настройке, многое автоматизированно
    * Простое взаимодействие с существующими инструментами Docker (вроде docker-compose)
    * Нетребователен к ресурсами
    * Легок в установке
  * Против
    * Функционал ограничен Docker API
    * Нету нормального мониторинга сервисов
* Kubernetes
  * За
    * Модульный
    * Гибкий в настройке
    * Мониторинги сервисов и более продуманный мониторинг самого кластера
    * Поддерживается и используется многими крупными компаниями на протяжении долгого срока
  * Против
    * Сложен в установке и настройке
    * Заметно более требователен к ресурсам
    * Несовместим с существующим Docker CLI и docker-compose

В целом Kubernetes больше нужен для больших проектов или компаний, где нужна гибкая настройка сервисов, хороший мониторинг.